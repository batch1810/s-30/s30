// Include express module
const express = require("express");
// Include mongoose module
// Mongoose is a package that allows creation of Schemas to model our data structure.
// Also has access to a number of methods for manipulating our databaese.
const mongoose = require("mongoose");

// Setup the express server
const app = express();

// List the port where the server will listen
const port = 3001;

// MongoDB Connection
// Connect to the database by passing in you connection string.
/*
	Syntax:
		mongoose.connect("<MongoDB Atlas Connection String>", {useNewUrlParser: true, useUnifiedTopology: true});
*/
mongoose.connect("mongodb+srv://prescy:prescy@course.qpyfo.mongodb.net/b183_to-do?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Set a notification for connection success or error with our database.
let db = mongoose.connection;

// If a connection error occurred, it will be output in the console.
// console.error.bind(console) allows us to print the error in the browser consoles and in the terminal.
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, a console message will be shown.
db.once("open", () => console.log("We're connected to the cloud database."));

// Mongoose Schema
// Determine the structure of the document to be written in the database.
// Schemas act as blueprints to our data.
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

// Mongoose Model
// Model uses Schemas and they act as the middleman from the server (JS code) to our database.
/*
	Syntax:
	const modelName = mongoose.model("collectionName", mongooseSchema);
*/
// Model must be in singular form and capitalize the first letter.

const User = mongoose.model("User", userSchema);

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Route for creating a user
// Activity
/*

	Instructions s30 Activity:
	1. Create a User schema.
	2. Create a User model.
	3. Create a POST route that will access the "/signup" route that will create a user.
	4. Process a POST request at the "/signup" route using postman to register a user.
	5. Create a git repository named S30.
	6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	7. Add the link in Boodle.

*/

//Register a user

/*
	Business Logic:

	1. Add a functionality to check if there are duplicate user
		- If the user already exists in the database, we return an error
		- If the user doesn't exist in the database, we add it in the database
	2. The user data will be coming from the request's body
		- Note: Make sure that the username and password from the request is not empty.
	3. Create a new User object with a "username" and "password" fields/properties
*/
app.post("/signup", (req, res) => {
	if (req.body.username == "" || req.body.password == ""){
		return res.send("Username and Password are required.");
	}

	User.findOne({username: req.body.username}, (err, result) => {
		if (result != null && req.body.username == result.username){
			// Retrn a message to the client
			return res.send("Duplicate user found!");
		}
		// If no document was found or no duplicate.
		else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, saveResult) => {
				// If there are errors in saving it will be displayed in the console.
				if (saveErr) {
					return console.error(saveErr)
				}
				// If no error found while creating the document it will be save in the database
				else {
					return res.status(200).send("New user created.");
				}
			});
		}
	})
})

// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`))