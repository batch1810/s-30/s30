// Include express module
const express = require("express");
// Include mongoose module
// Mongoose is a package that allows creation of Schemas to model our data structure.
// Also has access to a number of methods for manipulating our databaese.
const mongoose = require("mongoose");

// Setup the express server
const app = express();

// List the port where the server will listen
const port = 3001;

// MongoDB Connection
// Connect to the database by passing in you connection string.
/*
	Syntax:
		mongoose.connect("<MongoDB Atlas Connection String>", {useNewUrlParser: true, useUnifiedTopology: true});
*/
mongoose.connect("mongodb+srv://prescy:prescy@course.qpyfo.mongodb.net/b183_to-do?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Set a notification for connection success or error with our database.
let db = mongoose.connection;

// If a connection error occurred, it will be output in the console.
// console.error.bind(console) allows us to print the error in the browser consoles and in the terminal.
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, a console message will be shown.
db.once("open", () => console.log("We're connected to the cloud database."));

// Mongoose Schema
// Determine the structure of the document to be written in the database.
// Schemas act as blueprints to our data.
const taskSchema = new mongoose.Schema({
	// Name of the task
	name: String, // name: String is a shorthand for name: {type: String}
	// Status task (Complete, Pending, Incomplete)
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value.
		default: "Pending"
	}
});

// Mongoose Model
// Model uses Schemas and they act as the middleman from the server (JS code) to our database.
/*
	Syntax:
	const modelName = mongoose.model("collectionName", mongooseSchema);
*/
// Model must be in singular form and capitalize the first letter.

const Task = mongoose.model("Task", taskSchema);

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Route for createing a task
// Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error
		- If the task doesn't exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object

	Task is not duplicated:
	- result from the query not equal to null
	- req.body.name is equal to res.name
*/
app.post("/tasks", (req, res) => {
	// Call back functions in mongoose methods are programmed this way:
		// 1st param stores the error
		// 2nd param returns the result
	Task.findOne({name: req.body.name}, (err, result) => {
		if (result != null && req.body.name == result.name){
			// Retrn a message to the client
			return res.send("Duplicate task found!");
		}
		// If no document was found or no duplicate.
		else {
			// Create a new task and save to database
			let newTask = new Task({
				name: req.body.name
			});

			// The ".save" method will store the information to the database
			// Since the "newTask" was create/instantiated from the Task model that contains the Mongoose Schema, so it wil gain access to the save method.
			newTask.save((saveErr, saveResult) => {
				// If there are errors in saving it will be displayed in the console.
				if (saveErr) {
					return console.error(saveErr)
				}
				// If no error found while creating the document it will be save in the database
				else {
					return res.status(200).send("New task created.");
				}
			});
		}
	})
})

// Business Logic
/*
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if (err) {
			console.error(result)
		}
		else {
			return res.status(200).send(result)
		}
	})
})

// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`))